# ACCENTURE USECASE

# Machine Learning

The code for machine-learning purpose is located at `model/`.
Some minimal description can be found in script docstrings. 

Reader should follow scripts this way:
```
model/data.py
model/plots.py
model/data_reduced.py
model/preprocessing.py
model/train.py
model/evaluate/py
```

# Installation
***/!\ If you don't want to install web-app, skip this part.***

***All following commands should be run in the root directory of project***

***All script mentioned above should be run at least once before reading forward***

## Backend
Make sure *MongoDB* and *Elasticsearch* are installed and running on your system.
Listening ports should be defaults :

```
MongoDB: 27017
Elasticsearch: 9200
```

I recommend you use a virtual env to avoid messing your default environment.
I suggest `conda` from Anaconda python distrib for this.
```bash
conda create --name accenture python=3.6 python
source activate accenture
```

Install dependencies :
```bash
pip install -r requirements.txt
```

Once done, install the application manager:
```bash
pip install --editable .  # editable option is a workarround ...
```
Load raw data in data
```bash
accenture-app setup
```
This step might be messy since the IRIS geojson presents some miss-formated features. Though, the app should work anyway
https://www.data.gouv.fr/fr/datasets/contours-iris-2014/

## Frontend
Make sure `node` and `npm` are installed on your system
```bash
npm install
npm run build
```

## Run back and front
```bash
accenture-app run
```