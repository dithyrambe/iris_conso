from flask import Flask, render_template
from flask_cors import CORS

from .config import config

cfg = config.DevConfig

app = Flask(__name__, static_folder='../dist/static', template_folder='../dist')
app.config.from_object(config.DevConfig)
CORS(app)


@app.route('/')
def index():
    return render_template('index.html')

from .api import geo_ep, model_ep

app.register_blueprint(geo_ep)
app.register_blueprint(model_ep)
