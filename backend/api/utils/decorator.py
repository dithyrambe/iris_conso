from bson import ObjectId
from functools import wraps

from flask import jsonify


def stringify_objid(obj):
    """Return another json-like object where mongo ObjectId are converted to str"""
    if isinstance(obj, ObjectId):
        return str(obj)
    elif isinstance(obj, dict):
        return {k: stringify_objid(v) for k, v in obj.items()}
    elif isinstance(obj, list):
        return [stringify_objid(elt) for elt in obj]
    else:
        return obj


def mongojson(f):
    """Transform a json-like output into another json-like object where mongo ObjectId are converted to str"""
    @wraps(f)
    def wrapper(*args, **kwargs):
        json_like = f(*args, **kwargs)
        result = stringify_objid(json_like)
        try:
            return jsonify(result)
        except TypeError:
            return result
    return wrapper


def lonlat_to_latlon(coordinates):
    if not all((isinstance(elt, list) for elt in coordinates)):
        return coordinates[::-1]
    else:
        return [lonlat_to_latlon(elt) for elt in coordinates]


def leafletize_coord(obj):
    if isinstance(obj, dict) and 'geometry' in obj:
        type_ = obj['geometry']['type']
        coord = lonlat_to_latlon(obj['geometry']['coordinates'])

        new_obj = {k: v for k, v in obj.items() if k != 'geometry'}
        new_obj['geometry'] = dict(type=type_, coordinates=coord)

        return new_obj

    elif isinstance(obj, dict):
        return {k: leafletize_coord(v) for k, v in obj.items()}
    elif isinstance(obj, list):
        return [leafletize_coord(elt) for elt in obj]
    else:
        return obj


def leafletize(f):
    """Decorator to switch lon-lat coordinates into lat-lon. For leaflet purpose"""
    @wraps(f)
    def wrapper(*args, **kwargs):
        json_like = f(*args, **kwargs)
        return leafletize_coord(json_like)
    return wrapper
