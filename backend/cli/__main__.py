import click

from . import utils
from .. import app
from .. import cfg
from ..db.commons import setup_es, delete_index, get_conso_from_iris


@click.group()
def cli():
    pass


@cli.command(help="Run the web server")
@click.option('--debug', default=False, is_flag=True, help='Run in debug mode')
def run(debug):
    if not utils.check_es_connection():
        click.secho('Connection to ElasticSearch failed. Make sure service is up and listening at {}:{}'.format(
            cfg.ELASTIC_URL, cfg.ELASTIC_PORT
        ), err=True, fg='red')
        return False
    if not utils.check_mongo_connection():
        click.secho('Connection to MongoDB failed. Make sure service is up and listening at {}:{}'.format(
            cfg.MONGO_URL, cfg.MONGO_PORT
        ), err=True, fg='red')
        return False

    app.run(debug=debug)


@cli.command(help="Setup data base")
def setup():
    try:
        setup_es()
    finally:
        click.echo('Aggregating IRIS ... (long process)')
        get_conso_from_iris()


@cli.command(help="Delete Elasticsearch index")
def delete_es():
    delete_index()


if __name__ == '__main__':
    cli()
