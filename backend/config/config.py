class Config:
    DEBUG = True
    TESTING = False
    DATABASE_NAME = 'vue-db'
    IS_FAKE = False


class DevConfig(Config):
    SECRET_KEY = 'a441b15fe9a3cf56661190a0b93b9dec7d04127288cc87250967cf3b52894d11'

    MONGO_URL = 'localhost'
    MONGO_PORT = 27017
    MAX_DELAY = 500

    ELASTIC_URL = 'localhost'
    ELASTIC_PORT = '9200'
    ELASTIC_INDEX = 'accenture'

    COUNTRY_GEOJSON_LOCATION = 'data/geo/countries.geo.json'
    REGION_GEOJSON_LOCATION = 'data/geo/france_regions.geo.json'
    DEPT_GEOJSON_LOCATION = 'data/geo/france_departements.geo.json'
    MICROZONE_GEOJSON_LOCATION = 'data/geo/france_microzones.geo.json'
    COMMUNE_GEOJSON_LOCATION = 'data/geo/france_communes.geo.json'
    IRIS_GEOJSON_LOCATION = 'data/geo/france_iris_2014.geo.json'


class ProdConfig(Config):
    DEBUG = False
    MONGO_URL = 'localhost'
    MONGO_PORT = 27017


config = {
    'dev': DevConfig,
    'prod': ProdConfig,
    'test': DevConfig,
}
