from ... import app

from .levels import *

geo_levels = [
    {
        'name': 'region',
        'resource': Region,
        'data_location': app.config['REGION_GEOJSON_LOCATION'],
        'hierarchy': {
            'rank': 1,
            'parent_level': '',
            'current_level': 'region',
            'child_level': 'dept'
        }
    },
    {
        'name': 'dept',
        'resource': Department,
        'data_location': app.config['DEPT_GEOJSON_LOCATION'],
        'hierarchy': {
            'rank': 2,
            'parent_level': 'region',
            'current_level': 'dept',
            'child_level': 'commune'
        }
    },
    {
        'name': 'commune',
        'resource': Commune,
        'data_location': app.config['COMMUNE_GEOJSON_LOCATION'],
        'hierarchy': {
            'rank': 3,
            'parent_level': 'dept',
            'current_level': 'commune',
            'child_level': 'iris'
        }
    },
    {
        'name': 'iris',
        'resource': Iris,
        'data_location': app.config['IRIS_GEOJSON_LOCATION'],
        'hierarchy': {
            'rank': 4,
            'parent_level': 'commune',
            'current_level': 'iris',
            'child_level': ''
        }
    }
]

geo_levels_by_name = {
    level['name']: {k: v for k, v in level.items()}
    for level in geo_levels
}
