from ..base import ElasticModel


class GeoElasticModel(ElasticModel):
    _mapping = {
        "properties": {
            "geometry": {
                "type": "geo_shape",
                "tree": "quadtree",
                "precision": "100m"
            },
        }
    }

    @classmethod
    def get_intersect_with(cls, doc_type=None, _id=None, index=None, aggs=None):
        assert all((doc_type, _id)), "must specify type & id args"

        if index is None:
            index = cls._index

        query = {
            "query": {
                "bool": {
                    "filter": {
                        "geo_shape": {
                            "geometry": {
                                "indexed_shape": {
                                    "index": index,
                                    "type": doc_type,
                                    "id": _id,
                                    "path": "geometry"
                                }
                            }
                        }
                    }
                }
            },
        }

        if aggs:
            query['aggs'] = aggs

        res = cls.scan(query=query)
        return res

    @classmethod
    def put_mapping(cls):
        cls._es.indices.put_mapping(index=cls._index, doc_type=cls._doctype, body=cls._mapping)

    @classmethod
    def aggregate(cls, value, from_type=None):
        """Messy classmethod ..."""
        for record in cls.scan():
            r = cls._es.search(
                index=cls._index, doc_type=from_type, body={
                    "query": {
                        "bool": {
                            "filter": {
                                "geo_shape": {
                                    "geometry": {
                                        "indexed_shape": {
                                            "index": cls._index,
                                            "type": cls._doctype,
                                            "id": record['_id'],
                                            "path": "geometry"
                                        }
                                    }
                                }
                            }
                        }
                    },
                    "aggs": {
                        value: {
                            "sum": {
                                "field": value
                            }
                        }

                    }
                })

            cls._es.update(
                index=cls._index,
                doc_type=cls._doctype,
                id=record['_id'],
                body={"doc": {'conso': r['aggregations'][value]['value']}}
            )
