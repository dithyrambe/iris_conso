"""
Here are the data models AFTER feature selection from correlation matrix
"""
import json
from functools import wraps

from tqdm import tqdm
import pandas as pd


class Data:
    PATH_TO_DATA = None
    SEP = ';'
    ENCONDING = 'utf8'
    DTYPES = {}
    KEEP = []
    RENAME = {}

    _transform = []

    data = None

    @classmethod
    def prepare(cls, rename=False):
        cls.data = pd.read_csv(
            cls.PATH_TO_DATA,
            sep=cls.SEP,
            encoding=cls.ENCONDING,
            dtype=cls.DTYPES,
            usecols=cls.KEEP
        )

        while cls._transform:
            col, transformation = cls._transform.pop(0)
            cls.data[col] = transformation(cls.data[col])

        if rename:
            cls.rename()

    @classmethod
    def rename(cls):
        cls.data = cls.data.rename(columns=cls.RENAME)

    @classmethod
    def apply_to(cls, *columns):
        def decorator(f):
            @wraps(f)
            def wrapped(*args, **kwargs):
                new_col = f(*args, **kwargs)
                return new_col

            for col in columns:
                cls._transform.append((col, wrapped))
            return wrapped
        return decorator


class Conso(Data):
    PATH_TO_DATA = "raw_data/consommation-electrique-par-secteurs-dactivite.csv"
    DTYPES = {
        'Code IRIS': str,
        'Type IRIS': str,
        'Conso totale secteur résidentiel (MWh)': float
    }
    KEEP = [
        'Code IRIS',
        'Type IRIS',
        'Année',
        'Conso totale secteur résidentiel (MWh)'
    ]
    RENAME = {
        'Code IRIS': 'IRIS',
        'Conso totale secteur résidentiel (MWh)': 'conso'
    }


class Logements(Data):
    PATH_TO_DATA = 'raw_data/base-ic-logement-2014.csv'
    DTYPES = {
        'IRIS': str,
    }
    KEEP = [
        'IRIS',
        'P14_RP',
        'P14_NBPI_RPMAISON',
        'P14_NBPI_RPAPPART',
    ]


class Menages(Data):
    PATH_TO_DATA = 'raw_data/base-ic-couples-familles-menages-2014.csv'
    DTYPES = {
        'IRIS': str
    }
    KEEP = [
        'IRIS',
        'C14_MENPSEUL',
    ]


class Population(Data):
    PATH_TO_DATA = 'raw_data/base-ic-evol-struct-pop-2014.csv'
    DTYPES = {
        'IRIS': str
    }
    KEEP = [
        'IRIS',
    ]


class Centroids:
    PATH_TO_DATA = 'data/geo/france_iris_2014.geo.json'

    data = None

    @classmethod
    def prepare(cls):
        with open(cls.PATH_TO_DATA) as f:
            geojson = json.load(f)

        centroids = [
            {
                'IRIS': feat['properties']['dcomiris'],
                'lon': feat['properties']['geo_point_2d'][1],
                'lat': feat['properties']['geo_point_2d'][0],
            }
            for feat in tqdm(geojson['features'])
        ]

        cls.data = pd.DataFrame(centroids).drop_duplicates('IRIS')
