"""
This scripts outputs performance on the TEST SET
"""
from sklearn.externals import joblib
from sklearn.metrics import mean_squared_error
from math import sqrt
import pandas as pd

test = pd.read_csv('data_set/test.csv')

target = test['conso'].values
features = test.drop(['Année', 'IRIS', 'conso'], axis=1).values

rf = joblib.load('model/trained_regressor.pckl')
lr = joblib.load('model/trained_linear_model.pckl')

rf_predictions = rf.predict(features)
lr_predictions = lr.predict(features)

rf_mse = mean_squared_error(target, rf_predictions)
lr_mse = mean_squared_error(target, lr_predictions)

print("""
RESULT ON TEST SET :
------------------

RandomForest residual variance: {}
RandomForest explained variance: {}
RandomForest rmse: {} (MWh)
------------------------------------
LinearRegression residual variance: {}
LinearRegression explained variance: {}
LinearRegression rmse: {} (MWh)

""".format(
    rf_mse,
    1-rf_mse/target.var(),
    sqrt(rf_mse),

    lr_mse,
    1-lr_mse/target.var(),
    sqrt(lr_mse)
))
