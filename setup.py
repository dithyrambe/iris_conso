from setuptools import setup
from pip.req import parse_requirements

with open('README.md') as reader:
    readme = reader.read()

requirements = [str(ir.req) for ir in parse_requirements("requirements.txt", session=False)]

setup(
    name='accenture-app',
    version='0.0.0',
    description='accenture training',
    long_description=readme,
    url='https//gitlab.com/dithyrambe',
    author='Dithyrambe',
    author_email='dithyrambe@outlook.fr',
    license='Apache Software License',
    classifiers=[
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    keywords='',
    packages=['backend'],
    install_requires=requirements,
    entry_points={
        'console_scripts': [
            'accenture-app = backend.cli.__main__:cli'
        ]
    },
)
